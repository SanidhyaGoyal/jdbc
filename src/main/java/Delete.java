import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Delete {

	public static void main(String[] args) {
		try{  
			Class.forName("com.mysql.jdbc.Driver");  
			Connection con=DriverManager.getConnection(  
			"jdbc:mysql://localhost:3306/JDBC","root","root");  
			String qr="DELETE from emp WHERE id = 114";
			Statement stmt=con.createStatement();  
			int x=stmt.executeUpdate(qr);  
			if (x > 0)             
                System.out.println("Successfully Deleted"+x);             
            else            
                System.out.println("Deletion Failed"); 
              
            con.close(); 
        } 
        catch(Exception e) 
        { 
            System.out.println(e); 
        } 
	}

}
